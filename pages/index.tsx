import React, { useEffect, useState } from "react";
import { Col, Divider, Row } from 'antd';
import Product from "../components/Product/Product";
import httpService from "../services/httpService";
import style from "../style/Home.module.scss"
import config from "../config/config";

function HomePage({products}) {

    return (
        <div className={style.productList}>
            <Row>
                {
                    products.map((product, index) =>
                        <Col span={4} key={index}>
                            <Product product={product} />
                        </Col>
                    )
                }
            </Row>
        </div>
    )
}

export async function getServerSideProps() {

    const request = {
        method: "GET",
        url: config.endpoints.products
    }
   
    const res = await httpService.invokeApiPromise(request);
    const products = res?.data?.data?? [] ;
  
   
    return {
      props: {
        products
      },
    }
}
  
export default HomePage