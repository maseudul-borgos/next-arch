import React, { useEffect, useState } from "react";
import { MailOutlined, LockOutlined } from '@ant-design/icons';
import { Input, Button } from "antd";
import style from "../style/Login.module.scss"
import httpService from "../services/httpService";
import appService from "../services/appService";
import config from "../config/config";
import { ACCESS_TOKEN, REFRESH_TOKEN } from "../config/constants";

function Login() {

    const [isLoading, setIsLoading] = useState(false)
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    async function onSubmitClicked() {
        const request = { 
            method: "POST", 
            url: config.endpoints.login, 
            data: { 
                email,
                password
            } 
        };
        setIsLoading(true)
        httpService.invokeApi(request, (status, res) => {
            setIsLoading(false)
            if (status) {
                console.log("Login Successfull")
                console.log("Access Token", res?.data?.access_token)
                console.log("Refresh Token", res?.data?.refresh_token)
                appService.setKey(ACCESS_TOKEN, res?.data?.access_token)
                appService.setKey(REFRESH_TOKEN, res?.data?.refresh_token)
            } else {

            }
        })
    }

    return (
        <div className={style.mainDiv}>
            <h1>Login Page</h1>
            <Input size="large" type="email" placeholder="Email" prefix={<MailOutlined />} onChange={event => setEmail(event.target.value)} />
            <br />
            <br />
            <Input size="large" type="password" placeholder="Password" prefix={<LockOutlined />} onChange={event => setPassword(event.target.value)} />
            <br />
            <br />
            {isLoading ?
                <Button type="primary" loading>
                    Please Wait
                </Button>
                :<Button type="primary" onClick={onSubmitClicked}>
                    Login
                </Button>
            } 
           
        </div>
    )
}
  
export default Login