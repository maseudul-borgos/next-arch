import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import config from "../../config/config";
import httpService from "../../services/httpService";
import style from "../../style/Product.module.scss"

function ProductDetails() {

    const router = useRouter()
    const [product, setProduct] = useState<{[key: string]: any}>({});

    useEffect(() => {
        if(!router.isReady) return;
        const { productId } = router.query
        const request = {
            method: "GET",
            url: config.endpoints.products+`/${productId}`,
            isAuthenticated: true
        }
        httpService.invokeApi(request, (status, res) => {
            if (status) {
                setProduct(res?.data?? {})
            } else {

            }
        })
    }, [router.isReady])

    return (
        <div className={style.productDetails}>
            <h1>Product: {product?.title?? ""}</h1>
            <h1>Price: {product?.price?? ""}</h1>
            <h1>Description: {product?.description?? ""}</h1>
        </div>
    )
}
  
export default ProductDetails