import axios from "axios";
import { ACCESS_TOKEN } from "../config/constants";
import appService from "./appService";

let axiosInstance = axios.create({
    baseURL: process.env.NEXT_PUBLIC_BASE_URL,
    timeout: 60000,
});

class HTTPService {

    invokeApi = (request: any, callback: any) => {

      const config: any = {
        method: request.method,
        url: request.url,
        withCredentials: request?.withCredentials?? false
      };

      if (request.data !== undefined) {
        config.data = request.data;
      }

      if (request.params !== undefined) {
        config.params = request.params;
      }
      
      if (request.headers !== undefined) {
        config.headers = request.headers;
      } else {
        config.headers = {
          'Content-Type': 'application/json'
        };
      }

      if (request.responseType) {
        config.responseType = request.responseType;
      }

      if (request?.isAuthenticated?? false) {
        config.headers.Authorization = `Bearer ${appService.getKey(ACCESS_TOKEN)}`
      }

      axiosInstance
        .request(config)
        .then(res => {
          callback(true, res.data);
        })
        .catch(err => {
          callback(false, err ? (err.response ? err.response.data : err) : err);
        });

    };

    invokeApiPromise = (request: any):Promise<any> => {

      const config: any = {
        method: request.method,
        url: request.url,
        withCredentials: request?.withCredentials?? false
      };

      if (request.data !== undefined) {
        config.data = request.data;
      }

      if (request.params !== undefined) {
        config.params = request.params;
      }

      if (request.headers !== undefined) {
        config.headers = request.headers;
      } else {
        config.headers = {
          'Content-Type': 'application/json'
        };
      }

      if (request.responseType) {
        config.responseType = request.responseType;
      }

      if (request?.isAuthenticated?? false) {
        config.headers.Authorization = `Bearer ${appService.getKey(ACCESS_TOKEN)}`
      }

      return axiosInstance.request(config)

    };

}

const httpService = new HTTPService()
export default httpService
