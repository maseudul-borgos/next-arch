const routes = {
    homepage: '/',
    login: '/login',
    products: '/products'
}

export default routes