const baseUrl = process.env.NEXT_PUBLIC_BASE_URL;

const config = {
    baseUrl,
    endpoints: {
        products: 'products',
        login: 'auth/login'
    }
}

export default config