import Image from "next/image";
import Link from "next/link";
import React from "react";
import styles from "./Product.module.scss";
import productImage from "../../public/images/product.png"
import routes from "../../config/routes";

function Product({product}) {
    return (
        <div className={styles.postDiv}>
            <Image
                src={productImage}
                width={40}
                height={40}
            />
            <Link href={`${routes.products}/${product.slug}`}>
                <a>
                    {product.title}
                </a>
            </Link>
        </div>
    )
}
  
export default Product